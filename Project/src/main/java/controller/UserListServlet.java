package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userInfo");
        UserDao userDao = new UserDao();
        List<User> userList = userDao.findAll();
        
        try {
          if (!(user.equals(null))) {
            request.setAttribute("userList", userList);
            RequestDispatcher dispatcher =
                request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
            dispatcher.forward(request, response);
          }

        } catch (NullPointerException e) {
          response.sendRedirect("LoginServlet");
        }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String loginId = request.getParameter("user-loginid");
      String name = request.getParameter("user-name");
      String startDay = request.getParameter("date-start");
      String endDay = request.getParameter("date-end");
      UserDao userDao = new UserDao();
      List<User> userList = userDao.search(loginId, name, startDay, endDay);

      // if (userList == null) {
      // request.setAttribute("loginId", "nullです");
      // }

      request.setAttribute("loginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("startDay", startDay);
      request.setAttribute("endDay", endDay);

      request.setAttribute("userList", userList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
      // for(User user : userList) {
      // if(user.getLoginId().equals(loginId) && user.getName().indexOf(name) != -1) {
      //
      // }
      // }

	}

}
