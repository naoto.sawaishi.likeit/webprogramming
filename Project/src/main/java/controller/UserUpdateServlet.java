package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        HttpSession session = request.getSession();
        String ida = request.getParameter("id");
        User user = (User) session.getAttribute("userInfo");

        if (user == null) {
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
          dispatcher.forward(request, response);
          return;
        }

        int id = Integer.valueOf(ida);
        UserDao userDao = new UserDao();
        User user1 = userDao.findById(id);

        try {
          if (user1 == null) {
            RequestDispatcher dispatcher =
                request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
            dispatcher.forward(request, response);
            return;
          }

          request.setAttribute("user", user1);

          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
        } catch (NumberFormatException e) {
          response.sendRedirect("LoginServlet");
        }


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();
        PasswordEncorder pe = new PasswordEncorder();
        String ida = request.getParameter("user-id");
        int id = Integer.valueOf(ida);
        String loginId = request.getParameter("user-loginId");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String birthday = request.getParameter("birth-date");
        Date birthDate = null;
        String encodeStr;


        // if (password.equals("") && confirmPassword.equals("")) {
        // password = User.getPassword();
        // confirmPassword = User.getPassword();
        // }

        // エラー

        // if (!(birthday.equals(""))) {
        // birthDate = Date.valueOf(birthday);
        //
        // } else if (birthday.equals("")) {
        // birthDate = User.getBirthday();
        // }

        // if文で分岐 passwordとpassword-confirmが同じ
        if (!(password.equals(confirmPassword)) || name.equals("") || birthday.equals("")) {
          request.setAttribute("errMsg", "入力された内容は正しくありません。");
          User user = new User();
          user.setLoginId(loginId);
          user.setName(name);

          if (!(birthday.equals(""))) {
            birthDate = Date.valueOf(birthday);
            user.setBirthday(birthDate);
          }
          // if (birthday.equals("")) { // if文追加
          // request.setAttribute("birthday", birthday);
          // } else if (!(birthday.equals(""))) { // if文追加
          // birthDate = Date.valueOf(birthday);
          // user.setBirthday(birthDate);
          // }

          request.setAttribute("user", user);

          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
          return;
        } else if (!(password.equals(""))) {
          encodeStr = pe.encordPassword(password);

          userDao.update(id, encodeStr, birthday, name);

          response.sendRedirect("UserListServlet");
        } else if (password.equals("")) {
          userDao.update(id, birthday, name);

          response.sendRedirect("UserListServlet");
        }

	}

}
