package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddSurvlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userInfo");
        try {
          if (!(user.equals(null))) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
            dispatcher.forward(request, response);
          }

        } catch (NullPointerException e) {
          response.sendRedirect("LoginServlet");
        }







	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");
        // loginID, password, username, birthday を受け取る
        UserDao userDao = new UserDao();
        PasswordEncorder pe = new PasswordEncorder();
        String loginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String birthday = request.getParameter("birth-date");
        Boolean user = userDao.findLoginId(loginId); // tureなら例外処理

        try {
          if (!(password.equals(confirmPassword)) || password.equals("") || loginId.equals("")
              || birthday.equals("") || name.equals("") || confirmPassword.equals("")
              || user) {
            // Date birthDate = Date.valueOf(birthday);
            request.setAttribute("errMsg", "入力された内容は正しくありません。");
            // User user = new User();
            // user.setLoginId(loginId);
            // user.setName(name);
            // user.setBirthday(birthDate);
            request.setAttribute("loginId", loginId);
            request.setAttribute("name", name);
            request.setAttribute("birthday", birthday);
            // request.setAttribute("user", user);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
            dispatcher.forward(request, response);
          } else {
            String encodeStr = pe.encordPassword(password);
            userDao.insert(loginId, name, encodeStr, birthday);

            response.sendRedirect("UserListServlet");
          }
        } catch (IllegalArgumentException e) {
          response.sendRedirect("UserListServlet");
        }
        {

        }


	}

}
