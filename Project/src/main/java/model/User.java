package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class User implements Serializable {
  private int id;
  private String loginId;
  private String password;
  private Date birthday;
  private String name;
  private boolean isAdmin;
  private Timestamp createDate;
  private Timestamp updateDate;

  public User() {

  }

  public User(String loginId) {
    this.loginId = loginId;
  }

  public User(int id, String loginId, String password, Date birthday, String name, boolean isAdimn,
      Timestamp createDate, Timestamp updateDate) {
    this.id = id;
    this.loginId = loginId;
    this.password = password;
    this.birthday = birthday;
    this.name = name;
    this.isAdmin = isAdimn;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }




}
