package dao;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {

  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int uId = rs.getInt("id");
      String uLoginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String uPassword = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(uId, uLoginId, uPassword, birthDate, name, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public User findById(int id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int uId = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(uId, loginId, password, birthDate, name, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public Boolean findLoginId(String loginId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT login_id FROM user WHERE is_admin = false and login_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return false;
      }
      // int uId = rs.getInt("id");
      // String login_id = rs.getString("login_id");
      // String name = rs.getString("name");
      // Date birthDate = rs.getDate("birth_date");
      // String password = rs.getString("password");
      // boolean isAdmin = rs.getBoolean("is_admin");
      // Timestamp createDate = rs.getTimestamp("create_date");
      // Timestamp updateDate = rs.getTimestamp("update_date");
      return true;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {

      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE is_admin = false";// WHERE is_admin =false
                                                               // をつけることで管理者は表示されない
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, password, birthDate, name, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return userList;
  }

  public void insert(String loginId, String userName, String password, String birthday) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES (?, ?, ?, ?, now(), now())";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthday);
      pStmt.setString(4, password);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public void update(int id, String password, String birthday, String userName) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE id = ?";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthday);
      pStmt.setInt(4, id);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public void update(int id, String birthday, String userName) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, birthday);
      pStmt.setInt(3, id);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public void delete(String loginId) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE login_id = ?";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public List<User> search(String loginId, String name, String startDay, String endDay) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {

      conn = DBManager.getConnection();

      int i = 0;
      String[] str = new String[6];
      str[0] = null;
      String sql =
          "SELECT * FROM user WHERE is_admin = false";
      StringBuilder stringBuilder = new StringBuilder(sql);

      // SELECT * FROM user WHERE login_id = ? and birth_date >= ? and birth_date <= ? and name LIKE
      // ?
      // 本物
      // "SELECT * FROM user WHERE is_admin = false"; sql2
      // SELECT * FROM user WHERE login_id = ? loginidだけ
      // SELECT * FROM user WHERE name = ?
      // SELECT * FROM user WHERE birth_date = ? *
      // SELECT * FROM user WHERE birth_date = ? *
      // SELECT * FROM user WHERE name = ? and login_id = ?
      // SELECT * FROM user WHERE login_id = ? birth_date = ?

      // SELECT * FROM user WHERE birth_date <= ? and name LIKE ? 終わりの日にちと名前 始まりも同じ

      if (!(loginId.equals(""))) {
        stringBuilder.append(" and login_id = ?");
        i += 1; // 仮
        // stringの配列に入れてみる。
        str[i] = loginId;
      }
      if (!(name.equals(""))) {
        stringBuilder.append(" and name LIKE ?");
        i += 1;
        str[i] = name;
      }
      if (!(startDay.equals(""))) {
        stringBuilder.append(" and birth_date >= ?");
        i += 1;
        str[i] = startDay;
      }
      if (!(endDay.equals(""))) {
        stringBuilder.append(" and birth_date <= ?");
        i += 1;
        str[i] = endDay;
      }


      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());

      for (int cnt = 0; cnt <= i; cnt++) {

        if (str[cnt] == name) {
          pStmt.setString(cnt, "%" + name + "%");
        } else if (str[cnt] == loginId) {
          pStmt.setString(cnt, loginId);
        } else if (str[cnt] == startDay) {
          pStmt.setString(cnt, startDay);
        } else if (str[cnt] == endDay) {
          pStmt.setString(cnt, endDay);
        } else {
        }
      }

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String login_id = rs.getString("login_id");
        String Name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, login_id, password, birthDate, Name, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return userList;
  }

}
